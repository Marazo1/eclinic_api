package handlers

import (
	"strconv"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/Marazo1/eclinic_api/database"
	"gitlab.com/Marazo1/eclinic_api/models"
)

func FindDoctors(c *fiber.Ctx) error {
	db := database.DB.Where("")

	name := c.Query("name")
	if name != "" {
		db = db.Where("first_name LIKE ? OR last_name LIKE ?", "%"+name+"%", "%"+name+"%")
	}

	specialty := c.Query("specialty")
	if specialty != "" {
		db = db.Where("specialty LIKE ?", "%"+specialty+"%")
	}

	sort := c.Query("sort")
	order := c.Query("order")
	if sort != "" && (order == "asc" || order == "desc") {
		sortQuery := sort + order
		db = db.Order(sortQuery)
	}

	var doctors []models.Doctor
	db.Find(&doctors)

	return c.JSON(doctors)
}

func GetDoctorById(c *fiber.Ctx) error {
	var doctor models.Doctor
	database.DB.First(&doctor, c.Params("id"))

	if doctor.ID == 0 {
		return c.Status(400).JSON(fiber.Map{
			"message": "Doctor not found",
			"status":  400,
		})
	}

	return c.JSON(doctor)
}

func PostDoctor(c *fiber.Ctx) error {
	var doctor models.Doctor
	c.BodyParser(&doctor)
	createdPatient := database.DB.Create(&doctor)

	if createdPatient.Error != nil {
		return c.Status(400).JSON(fiber.Map{
			"message": "Wrong data",
			"status":  400,
		})
	}

	return c.JSON(&doctor)
}

func UpdateDoctor(c *fiber.Ctx) error {
	var doctor models.Doctor
	database.DB.First(&doctor, c.Params("id"))

	if doctor.ID == 0 {
		return c.Status(400).JSON(fiber.Map{
			"message": "Doctor not found",
			"status":  400,
		})
	}

	c.BodyParser(&doctor)
	id, _ := strconv.ParseUint(c.Params("id"), 10, 64)
	doctor.ID = uint(id)
	database.DB.Save(&doctor)

	return c.JSON(&doctor)
}

func DeleteDoctor(c *fiber.Ctx) error {
	var doctor models.Doctor
	database.DB.First(&doctor, c.Params("id"))

	if doctor.ID == 0 {
		return c.Status(400).JSON(fiber.Map{
			"message": "Doctor not found",
			"status":  400,
		})
	}

	database.DB.Unscoped().Delete(&models.Doctor{}, c.Params("id"))

	return c.JSON(fiber.Map{
		"message":   "User deleted successfully",
		"patientId": c.Params("id"),
		"status":    200,
	})
}
