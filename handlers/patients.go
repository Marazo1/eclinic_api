package handlers

import (
	"strconv"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/Marazo1/eclinic_api/database"
	"gitlab.com/Marazo1/eclinic_api/models"
)

func GetPatientById(c *fiber.Ctx) error {
	var patient models.Patient
	database.DB.First(&patient, c.Params("id"))

	if patient.ID == 0 {
		return c.Status(400).JSON(fiber.Map{
			"message": "Patient not found",
			"status":  400,
		})
	}

	return c.JSON(patient)
}

func PostPatient(c *fiber.Ctx) error {
	var patient models.Patient
	c.BodyParser(&patient)
	createdPatient := database.DB.Create(&patient)

	if createdPatient.Error != nil {
		return c.Status(400).JSON(fiber.Map{
			"message": "Wrong data",
			"status":  400,
		})
	}

	return c.JSON(&patient)
}

func UpdatePatient(c *fiber.Ctx) error {
	var patient models.Patient
	database.DB.First(&patient, c.Params("id"))

	if patient.ID == 0 {
		return c.Status(400).JSON(fiber.Map{
			"message": "Patient not found",
			"status":  400,
		})
	}

	c.BodyParser(&patient)
	id, _ := strconv.ParseUint(c.Params("id"), 10, 64)
	patient.ID = uint(id)
	database.DB.Save(&patient)

	return c.JSON(&patient)
}

func DeletePatient(c *fiber.Ctx) error {
	var patient models.Patient
	database.DB.First(&patient, c.Params("id"))

	if patient.ID == 0 {
		return c.Status(400).JSON(fiber.Map{
			"message": "Patient not found",
			"status":  400,
		})
	}

	database.DB.Unscoped().Delete(&models.Patient{}, c.Params("id"))

	return c.JSON(fiber.Map{
		"message":   "User deleted successfully",
		"patientId": c.Params("id"),
		"status":    200,
	})
}
