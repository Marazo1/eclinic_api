package models

import (
	"gorm.io/gorm"
)

type Doctor struct {
	gorm.Model

	DNI           string `gorm:"size:20;uniqueIndex" json:"dni"`
	Email         string `gorm:"size:100;not null;uniqueIndex" json:"email"`
	FirstName     string `gorm:"size:50" json:"firstName"`
	LastName      string `gorm:"size:50" json:"lastName"`
	ContactNumber string `gorm:"size:20" json:"contactNumber"`
	Specialty     string `gorm:"size:150" json:"specialty"`
}
