package models

import (
	"time"

	"gorm.io/gorm"
)

type Gender string
type BloodType string

const (
	Female Gender = "Female"
	Male   Gender = "Male"
	Other  Gender = "Other"
)

const (
	AP  BloodType = "A+"
	AN  BloodType = "A-"
	BP  BloodType = "B+"
	BN  BloodType = "B-"
	ABP BloodType = "AB+"
	ABN BloodType = "AB-"
	OP  BloodType = "O+"
	ON  BloodType = "O-"
)

type Patient struct {
	gorm.Model

	DNI           string    `gorm:"size:20;uniqueIndex" json:"dni"`
	Email         string    `gorm:"size:100;not null;uniqueIndex" json:"email"`
	FirstName     string    `gorm:"size:50" json:"firstName"`
	LastName      string    `gorm:"size:50" json:"lastName"`
	DateOfBirth   time.Time `json:"dateOfBirth"`
	Address       string    `gorm:"size:255" json:"address"`
	ContactNumber string    `gorm:"size:20" json:"contactNumber"`
	Gender        Gender    `gorm:"type:enum('Male', 'Female', 'Other')" json:"gender"`
	BloodType     BloodType `gorm:"type:enum('A+', 'A-', 'B+', 'B-', 'AB+', 'AB-', 'O+', 'O-')" json:"bloodType"`
	Allergies     []string  `gorm:"serializer:json" json:"allergies"`
}
