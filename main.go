package main

import (
	"log"
	"os"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/logger"
	"github.com/joho/godotenv"
	"gitlab.com/Marazo1/eclinic_api/database"
	"gitlab.com/Marazo1/eclinic_api/handlers"
)

func init() {
	err := godotenv.Load(".env")
	database.DBConnection()
	// database.DB.Migrator().DropTable(models.Patient{})
	// database.DB.AutoMigrate(models.Patient{})

	if err != nil {
		log.Fatal("Error loading .env file")
	}
}

func main() {
	PORT := os.Getenv("PORT")
	app := fiber.New()

	app.Use(logger.New())

	patients := app.Group("/api/patients")
	doctors := app.Group("/api/doctors")

	patients.Get("/:id", handlers.GetPatientById)
	patients.Post("", handlers.PostPatient)
	patients.Put("/:id", handlers.UpdatePatient)
	patients.Delete("/:id", handlers.DeletePatient)
	doctors.Get("/:id", handlers.GetDoctorById)
	doctors.Get("", handlers.FindDoctors)
	doctors.Post("", handlers.PostDoctor)
	doctors.Put("/:id", handlers.UpdateDoctor)
	doctors.Delete("/:id", handlers.DeleteDoctor)

	app.Listen(PORT)
}
