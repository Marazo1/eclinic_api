package database

import (
	"log"
	"os"

	"github.com/joho/godotenv"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

var DB *gorm.DB

func init() {
	err := godotenv.Load(".env")

	if err != nil {
		log.Fatal("Error loading .env file")
	}
}

func DBConnection() {
	var error error
	URL := os.Getenv("DATABASE_URL")
	DB, error = gorm.Open(mysql.Open(URL), &gorm.Config{})

	if error != nil {
		log.Fatal(error)
	}
}
